package main;

import resource.StaticResource;

/** Класс представляет собой корм разбрасываемый во время игры */
public class Pie extends GameObject {

    /** В конструкторе случайным образом задается размер получаемой энергии агентом
    * Генерируется случаный размер объекта
    */
    public Pie() {
        energy = random.nextInt(StaticResource.maxEnergyPie) + StaticResource.minEnergyPie;
        size = random.nextInt(StaticResource.maxSizePie) + StaticResource.minSizePie;
        min = size / 2;
        max = StaticResource.mapSize - min;
        fixPosition(x, y);
    }


    /** Коректирует данные координат, чтобы объект не выходил за пределы карты
     * @see #fixPosition(double, double)
     */
    @Override
    public void fixPosition(double x, double y) {
        if (x > max)
            x = max;
        else if (x < min)
            x = min;

        if (y > max)
            y = max;
        else if (y < min)
            y = min;

        setPosition(x, y);
    }

    @Override
    public String toString() {
        return "x: " + x + " y: " + y + " size: " + size;
    }
}


