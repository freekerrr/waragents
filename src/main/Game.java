package main;

import resource.PropertiesGame;
import runnable.Security;

public class Game {
    private static Game game = new Game();

    public static Game getInstance() {
        if (game == null)
            game = new Game();
        return game;
    }

    /** Идентификатор текущей игры */
    private int gameId = 0;
    /** Объект контролирующий взаимодействие объектов на карте */
    private Security security;
    /** Значание текущего состояния игры */
    public boolean isGame;

    /** Базовый конструктор класса, в нем происходит считывание файла config.properties,
     * создается экземпляр класса Security и увеличивается счетчик игры на единицу.
     */
    private Game() {
        PropertiesGame.setGameProperties();
        security = new Security();
        gameId++;
    }

    /* Запускает игровой процесс, ставит состояние игры в значение true */
    public void start() {
        isGame = true;
        security.start();
    }

    /** Останавливает процесс игры */
    public void stop() {
        isGame = false;
        security.stopGame();
    }

    public int getGameId() {
        return gameId;
    }

    /** Запускает процесс игры заново */
    public void restart() {
        game = new Game();
    }
}
