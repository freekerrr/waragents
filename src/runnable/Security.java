package runnable;

import main.Game;
import main.GameObject;
import main.Pie;
import resource.DataBase;
import resource.PropertiesGame;
import resource.StaticResource;
import socket.WEBSocket;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Класс отвечающий за поведение и взаимодействие объектов на поле
 */
public class Security implements Runnable {
    /**
     * Поток в котором работает класс
     */
    private Thread t;
    /**
     * Контейнер Агентов
     */
    private ArrayList<Agent> agents;
    /**
     * Контейнер пирогов
     */
    private ArrayList<Pie> pies;

    /**
     * Внутренний класс, отвечающий за производство пирогов
     */
    private PieController pieController;

    /**
     * Объект отвечающий за получение агентов в соответствии с заданными настройками
     */
    private PropertiesGame pg;

    /**
     * Базовый конструктор
     */
    public Security() {
        pieController = new PieController();
        agents = new ArrayList<>();
        pg = new PropertiesGame();
    }

    /**
     * Создание и запуск Агентов по данным из файла ресурсов
     * Старт контролера корма
     * Старт основного потока
     */
    public void start() {
        FileInputStream input = null;
        try {
            input = new FileInputStream("D:\\IntelliJ IDEA Ultimate\\warAgents\\config.properties");
            pg.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }

        agents.add(pg.getAgent(0));
        agents.add(pg.getAgent(0));
        agents.add(pg.getAgent(1));
        agents.add(pg.getAgent(1));
        agents.add(pg.getAgent(2));
        agents.add(pg.getAgent(2));
        startAgents();

        pieController.start();

        t = new Thread(this);
        t.start();
    }

    /**
     * Запуск основного цикла игры
     */
    @Override
    public void run() {
        while (Game.getInstance().isGame) {
            try {
            for (int i = 0; i < agents.size(); i++) {
                if (!agents.get(i).isAlive())
                    agents.remove(agents.get(i));
                else {
                    for (int j = 0; j < pies.size(); j++)
                        interactionPie(agents.get(i), pies.get(j));

                    for (int m = 0; m < agents.size(); m++)
                        if (!agents.get(i).equals(agents.get(m)))
                            interactionAgent(agents.get(i), agents.get(m));
                }
            }
                WEBSocket.updateGame(agents, pies);
            if(agents.size() == 1) {
                stopGame();
            }
                Thread.sleep(StaticResource.timeToMove);
            } catch (InterruptedException | IOException e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Остановка игры
     * Запись выживших Агентов в базу данных
     */
    public void stopGame() {
        stopAgents();
        DataBase.addAgents(agents);
        pieController.stop();
        Game.getInstance().isGame = false;
    }

    /**
     * Проверка на соприкоснование двух Агентов
     * Взаимодействие двух Агентов, после получения урона Агенты меняют траекторию
     * @param radiant первый Агент
     * @param dire второй Агент
     */
    private void interactionAgent(Agent radiant, Agent dire) {
        if (distance(radiant, dire) <= (radiant.getSize() + dire.getSize()) / 2) {
            radiant.giveDmg(dire.getDmg());
            dire.giveDmg(radiant.getDmg());
            radiant.changeDir();
            dire.changeDir();
        }
    }

    /**
     * Проверка на соприкоснование Агента и пирога
     * Взаимодейстиве Агента с пирогом, пополнение энергии от пирога
     * @param agent Агент участвующий во взаимодействии
     * @param pie Пирог столкнувшийся с Агентом
     */
    private void interactionPie(Agent agent, Pie pie) {
        if (distance(agent, pie) <= (pie.getSize() + agent.getSize()) / 2) {
            agent.eat(pie);
            pies.remove(pie);
        }
    }


    /**
     * Метод возвращает расстояние между двумя объектами
     * @param object1 первый объект
     * @param object2 второй объект
     * @return расстояния
     */
    private double distance(GameObject object1, GameObject object2) {
        return Math.sqrt(Math.pow(object2.getX() - object1.getX(), 2) + Math.pow(object2.getY() - object1.getY(), 2));
    }

    /**
     * Метод останавливает жизненный цикл всех агентов
     */
    private void stopAgents() {
        for (int i = 0; i < agents.size(); i++) {
            agents.get(i).stop();
        }
    }

    /**
     * Метод запускает жизненный цикл всех агентов
     */
    private void startAgents() {
        for (int i = 0; i < agents.size(); i++) {
            agents.get(i).start();
        }
    }

    public void setAgents(ArrayList<Agent> agents) {
        this.agents = agents;
    }
    public void setPies(ArrayList<Pie> pies) {
        this.pies = pies;
    }
    public ArrayList<Agent> getAgents() {
        return agents;
    }
    public ArrayList<Pie> getPies() {
        return pies;
    }

    /**
     * Внутриний класс отвечающий за распределение пирогов на поле
     */
    private class PieController implements Runnable {
        /**
         * Поток в котором работает котролер
         */
        Thread pc;
        /**
         * Состояние процесса выпечки
         *  true - продолжать выпечку
         *  false - прекратить выпечку
         */
        private boolean cook;

        /**
         * Базовый конструктов создающий массив пирогов
         */
        public PieController() {
            pies = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                pies.add(new Pie());
            }
        }

        /**
         * Метод запускает жизненный цикл контроллера
         */
        private void start() {
            cook = true;
            pc = new Thread(this);
            pc.start();
        }

        /**
         * Метод останавливает жизненный цикл контроллера
         */
        private void stop() {
            cook = false;
        }

        /**
         * Метод отвечающий за распространение пирогов
         */
        @Override
        public void run() {
            while (cook) {
                pies.add(new Pie());
                try {
                    Thread.sleep(800);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}

