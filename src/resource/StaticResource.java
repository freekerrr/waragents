package resource;

public class StaticResource {

    // параметры агента
    static public double energyLost = 0.5;
    static public double agentSize = 30;
    static public double agentSpeed = 30;

    // параметры пирожка
    public static int minSizePie = 15;
    public static int maxSizePie = 30;
    public static int minEnergyPie = 10;
    public static int maxEnergyPie = 30;

    // game
    static public int mapSize = 500;
    public static long timeToMove = 10;

    // база данных
    static public String dbUrl = "jdbc:mysql://localhost:3306/game?";
    static public String login = "user=root";
    static public String password = "&password=root";

}
