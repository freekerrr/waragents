package resource;

import runnable.Agent;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

/**
 * Класс отвечающий за считывание файла настроект игры
 */
public class PropertiesGame extends Properties {
    private static PropertiesGame pg = new PropertiesGame();

    public String getProperty(String group, String key) {
        return getProperty(group + '.' + key);
    }

    public static void setGameProperties() {

        try{
            pg.load(new FileInputStream("D:\\IntelliJ IDEA Ultimate\\warAgents\\config.properties"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        StaticResource.agentSize = Double.valueOf(pg.getProperty("agentSize"));
        StaticResource.agentSpeed = Double.valueOf(pg.getProperty("agentSpeed"));
        StaticResource.energyLost = Double.valueOf(pg.getProperty("energyLost"));
        StaticResource.minSizePie = Integer.valueOf(pg.getProperty("minSizePie"));
        StaticResource.maxSizePie = Integer.valueOf(pg.getProperty("maxSizePie"));
        StaticResource.minEnergyPie = Integer.valueOf(pg.getProperty("minEnergyPie"));
        StaticResource.maxEnergyPie = Integer.valueOf(pg.getProperty("maxEnergyPie"));
        StaticResource.dbUrl = pg.getProperty("dbUrl");
        StaticResource.login = pg.getProperty("login");
        StaticResource.password = pg.getProperty("password");
        StaticResource.timeToMove = Integer.valueOf(pg.getProperty("timeToMove"));

    }

    public Agent getAgent(int group) {
        double speed = Double.valueOf(pg.getProperty("group" + group, "agentSpeed"));
        double size = Double.valueOf(pg.getProperty("group" + group, "agentSize"));
        double damage = Double.valueOf(pg.getProperty("group" + group, "agentDmg"));

        return new Agent(group, speed, size, damage);
    }

}
