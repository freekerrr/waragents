package resource;

import runnable.Agent;

import java.sql.*;
import java.util.ArrayList;

/**
 * Класс отвечающий за подключение к базе данных и запись выживших Агентов в нее
 */
public class DataBase {

    /**
     * Подключение к базе данных
     * @return <code>Connection</code>
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see Connection
     */
    private static Connection getConn() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");

        String dbUrl = StaticResource.dbUrl + StaticResource.login + StaticResource.password;

        Connection connect = DriverManager.getConnection(dbUrl);

        return connect;
    }

    /**
     * Получает максимальный идентификатор текущей игры
     * @return Максимальный id
     */
    public static int getMaxID() {
        try {
            Connection conn = getConn();

            String query = "SELECT MAX(gameId) as gameId FROM agents";

            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(query);

            int maxID = 0;
            while (rs.next()) {
                if (rs.getString("gameId") == null) {
                    maxID = 0;
                } else {
                    maxID = rs.getInt("gameId");
                }
            }
            st.close();
            return maxID;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Добавляет Агентов в базу данных
     * @param agents массив Агетов
     */
    public static void addAgents(ArrayList<Agent> agents) {
        try {
            Connection conn = getConn();

            String query = "insert into agents (id, gameId, x, y, `group`, energy)"
                    + " values (DEFAULT , ?, ?, ?, ?,?)";


            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setInt(1, getMaxID() + 1);

            for (Agent agent : agents) {
                preparedStmt.setDouble(2, agent.getX());
                preparedStmt.setDouble(3, agent.getY());
                preparedStmt.setInt(4, agent.getGroup());
                preparedStmt.setDouble(5, agent.getEnergy());

                preparedStmt.execute();
            }
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
