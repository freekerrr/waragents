package socket;

import main.Game;
import main.Pie;
import runnable.Agent;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Класс отвечающий за соединение веб клиента с серверной частью
 */
@ServerEndpoint("/control")
public class WEBSocket {

    /**
     * Текущая сессия подключения
     */
    private static Session peer;

    /**
     * Метод обрабатывающий поступающие сообщение
     * @param message текст сообщения
     */
    @OnMessage
    public void onMessage(String message) {
        if(message.equals("Start")){
            if (Game.getInstance().getGameId() > 0)
                Game.getInstance().restart();
            Game.getInstance().start();
        }else if (message.equals("Stop")){
            Game.getInstance().stop();
        }
    }

    /**
     * Метод срабатывает при закрытии сессии
     * @param session
     */
    @OnClose
    public void close(Session session) {
        System.out.println("close");
    }

    /**
     * Метод присваевает текущую сессию
     * @param s сессия
     */
    @OnOpen
    public void onOpen(Session s) {
        peer = s;
    }

    /**
     * Метод отвечающий за обновление изображения объектов на карте
     * @param agents текущие агенты
     * @param pies текущие пироги
     * @throws IOException Если вход или выход
     *                       Исключено событие
     */
    public static void updateGame(ArrayList<Agent> agents, ArrayList<Pie> pies) throws IOException {
        String json = gameToJson(agents,pies);
        peer.getBasicRemote().sendText(json);
    }

    /**
     * Преобразует контейнер Агентов и пирогов в тект JSON
     * @param agents контейнер агентов
     * @param pies контейнер пирогов
     * @return строку JSON
     */
    public static String gameToJson(ArrayList<Agent> agents, ArrayList<Pie> pies){
        String json = "{\"agents\": [";
        try {
            ArrayList<String> agentsStr = new ArrayList<>();
            for (Agent agent : agents) {
                if (agent.isAlive()) {
                    agentsStr.add("{\"x\": " + agent.getX()
                            + ", \"y\": " + agent.getY()
                            + ", \"group\": " + agent.getGroup()
                            + ", \"energy\": " + agent.getEnergy()
                            + ", \"size\": " + agent.getSize()
                            + "}");
                }
            }
            json += String.join(",", agentsStr);
            json += "],\"foods\": [";

            ArrayList<String> foodStr = new ArrayList<>();
            for (Pie pie : pies) {
                foodStr.add("{\"x\": " + pie.getX() +
                        ", \"y\": " + pie.getY() +
                        ", \"size\": " + pie.getEnergy() +
                        ", \"energy\": " + pie.getEnergy() + "}");
            }
            json += String.join(",", foodStr);
            json += "], \"isGame\": " + Game.getInstance().isGame + "}";

        } catch (NoClassDefFoundError ex) {
            ex.printStackTrace();
        }

        return json;
    }

}
